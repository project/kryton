# KRYTON - atomic theme

- It respects Brad Frost atomic design: https://bradfrost.com/blog/post/atomic-web-design/

# CUSTOM THEME

1. Copy folder: themes/contrib/kryton -> themes/custom/kryton

2. Install NPM

	You have to install Node JS (https://nodejs.org) to your computer.

	```bash
	> cd themes/custom/kryton
	> npm install
	```

3. Edit kryton theme description for better orientation.

	themes/custom/kryton/kryton.info.yml

	```bash
	description: 'Custom theme'
	```

4. Ready for enable custom theme.

# GENERATE THEME STYLES

Use gulp to generate and update styles.

```bash
> cd themes/custom/kryton
> gulp
```

# CODING STANDARDS

STYLELINT
```bash
npx stylelint "css/*.css" --config .stylelintrc-css.json
npx stylelint "sass/**/*.scss" --config .stylelintrc-scss.json
```

PHPCS
```bash
phpcs --standard=Drupal --extensions=php,json,theme,css,scss,md,yml,js .
```
