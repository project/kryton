/**
 * @file
 * Suport for collapse search block.
 */

(function ($, Drupal) {
  $("#header .block-search .close-search").click(function () {
    if($(this).parent().hasClass("open")){
      $(this).parent().removeClass("open");
    }
    else{
      $(this).parent().addClass("open");
    }
  });
})(window.jQuery, window.Drupal);
